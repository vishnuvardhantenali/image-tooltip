﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;


namespace ImageToolTip
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public int ImageFilesCount = 0;
        public bool bStart { get; set; } = false;
        public bool bClick { get; set; } = false;

        /// <summary>
        /// Load the Images
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            bClick = true;  
            LoadImages();
        }

        // Create image tooltips dynamically
        public void LoadImages()
        {            
            try
            {
                if (!bStart || !bClick) 
                    return;

                ImageFilesCount = 0;
                wPanel.Children.Clear();
                Cursor = Cursors.Wait;
                string imgPath = txtFolderPath.Text;

                // Get all files in the directory with image extensions
                string[] imageExtensions = { ".jpg", ".jpeg", ".png", ".gif", ".bmp" }; // Add more extensions as needed
                string[] imageFiles = Directory.GetFiles(imgPath, "*.*").
                    Where(file => imageExtensions.Contains(Path.GetExtension(file), StringComparer.OrdinalIgnoreCase)).ToArray();

                foreach (string file in imageFiles)
                {
                    ImageFilesCount++;
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.UriSource = new Uri(file);
                    bitmap.EndInit();

                    Image image = new Image();
                    
                    if (rbLarge.IsChecked == true)
                    {
                        image.Width = 255;
                        image.Height = 255;
                    }
                    else if (rbMedium.IsChecked == true) 
                    {
                        image.Width = 200;
                        image.Height = 200;
                    }
                    else
                    {
                        image.Width = 155;
                        image.Height = 155;
                    }

                    image.Source = bitmap;
                    SetImageTooltip(image, file);

                    // Create the Border control
                    Border border = new Border();
                    border.Padding = new Thickness(1);
                    border.Margin = new Thickness(5);
                    border.BorderBrush = Brushes.Black;
                    border.BorderThickness = new Thickness(1);
                    border.Child = image;

                    wPanel.Children.Add(border);
                }
                lblFilesCount.Content=ImageFilesCount.ToString();
            }            
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        /// <summary>
        /// Setting the Image ToolTip to the Images
        /// </summary>
        /// <param name="element"></param>
        /// <param name="imagePath"></param>
        private void SetImageTooltip(UIElement element, string imagePath)
        {
            try
            {
                ScrollViewer scroller = new ScrollViewer();

                // Create a StackPanel for the ToolTip content
                StackPanel stackPanel = new StackPanel();

                // Create a Label for the ToolTip Heading content
                Label label = new Label();
                label.Content = Path.GetFileName(imagePath);
                label.FontWeight = FontWeights.Bold;
                stackPanel.Children.Add(label);

                // Create an Image for the ToolTip content
                Image image = new Image();
                image.Stretch = Stretch.Fill;
                image.Source = new BitmapImage(new Uri(imagePath, UriKind.Absolute));
                stackPanel.Children.Add(image);
                
                // Retrieve Image details
                BitmapImage bitmapImage = image.Source as BitmapImage;
                if (bitmapImage != null)
                {
                    int width = bitmapImage.PixelWidth;
                    int height = bitmapImage.PixelHeight;                    
                    
                    // Create another Label for the ToolTip Details content
                    Label label2 = new Label();
                    label2.Content = $"Image Details: Width: {width}; Height: {height}";
                    stackPanel.Children.Add(label2);
                }
                scroller.Content = stackPanel;
                ToolTipService.SetShowOnDisabled(element, true);
                ToolTipService.SetToolTip(element, scroller);
            }
            catch
            {
                //ignore error
            }
        }
        public double GetBoundingBoxDimension(ThumbSize currentThumbnailSize)
        {
            Double boundingboxDimension = 0;
            try
            {
                switch (currentThumbnailSize)
                {
                    case ThumbSize.Small:
                        {
                            boundingboxDimension = (System.Windows.SystemParameters.PrimaryScreenHeight * 12) / 100;
                        }
                        break;
                    case ThumbSize.Medium:
                        {
                            boundingboxDimension = (System.Windows.SystemParameters.PrimaryScreenHeight * 20) / 100;
                        }
                        break;
                    case ThumbSize.Large:
                        {
                            boundingboxDimension = (System.Windows.SystemParameters.PrimaryScreenHeight * 28) / 100;
                        }
                        break;
                    default: //large                    
                        {
                            boundingboxDimension = (System.Windows.SystemParameters.PrimaryScreenHeight * 28) / 100;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
            }

            return boundingboxDimension;
        }

        /// <summary>
        /// Small Thumbnail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rbSmall_Checked(object sender, RoutedEventArgs e)
        {
            LoadImages();
            bStart = true;
        }

        /// <summary>
        /// Medium Thumbnail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rbMedium_Checked(object sender, RoutedEventArgs e)
        {
            LoadImages();
            bStart = true;
        }

        /// <summary>
        /// Large Thumbnail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rbLarge_Checked(object sender, RoutedEventArgs e)
        {
            LoadImages();
            bStart = true;
        }       
    }

    public enum ThumbSize
    {
        Small,
        Medium,
        Large
    };
}
